package com.epam.servlets;

import com.epam.servlets.exception.PropertiesException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertiesManager {
    private final Properties PROPERTIES = new Properties();
    private final InputStream INPUT_STREAM;

    public PropertiesManager(String fileName) {
        INPUT_STREAM = getClass().getResourceAsStream(fileName);
        if (INPUT_STREAM == null) throw new PropertiesException("File \"" + fileName + "\" not found!");
        try {
            PROPERTIES.load(INPUT_STREAM);
        } catch (IOException ex) {
            throw new PropertiesException(ex);
        }
    }

    public int getInt(String key) throws PropertiesException {
        return Integer.parseInt(getString(key));
    }

    public String getString(String key) throws PropertiesException {
        ///try {
            baseCheck(key);
            //return PROPERTIES.getProperty(key);
            return new String(PROPERTIES.getProperty(key).getBytes(StandardCharsets.ISO_8859_1));
        /*} catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/
        //return null;
    }

    public Properties getProperties(String... keys) throws PropertiesException {
        if (keys.length == 0) {
            return PROPERTIES;
        }
        Properties answer = new Properties();
        for (String key : keys) {
            answer.setProperty(key, getString(key));
        }
        return answer;
    }

    private void baseCheck(String... keys) throws PropertiesException {
        for (String key : keys) {
            if (key == null) throw new PropertiesException("Each parameter must be not null!");
        }
        if (INPUT_STREAM == null) throw new PropertiesException("Properties file not loaded!");
    }
}
