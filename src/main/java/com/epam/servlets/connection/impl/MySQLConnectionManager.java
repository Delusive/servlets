package com.epam.servlets.connection.impl;

import com.epam.servlets.PropertiesManager;
import com.epam.servlets.connection.IConnectionManager;
import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class MySQLConnectionManager implements IConnectionManager {
    private static final PropertiesManager propertiesManager = new PropertiesManager("/servlets.properties");
    private static final MySQLConnectionManager instance = new MySQLConnectionManager(propertiesManager.getProperties("connection.mysql.url", "connection.mysql.username", "connection.mysql.password"));
    private final MysqlDataSource dataSource = new MysqlDataSource();


    private MySQLConnectionManager(Properties props) {
        dataSource.setUrl(props.getProperty("connection.mysql.url"));
        dataSource.setUser(props.getProperty("connection.mysql.username"));
        dataSource.setPassword(props.getProperty("connection.mysql.password"));
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public static MySQLConnectionManager getInstance() {
        return instance;
    }
}
