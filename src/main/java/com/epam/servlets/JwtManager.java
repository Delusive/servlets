package com.epam.servlets;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.epam.servlets.entity.User;
import com.epam.servlets.exception.InvalidTokenException;

import java.util.Date;

public class JwtManager {
    private static final String SECRET = "LmNHFWwmbHTV9sMvaDV2vX642VZ8jekJ6";
    private static final int EXPIRES_MINS = 5;
    private static final Algorithm algorithm = Algorithm.HMAC256(SECRET);

    public static String generateTokenForUser(User user) {
        return JWT.create()
                .withClaim("id", user.ID)
                .withClaim("name", user.NAME)
                .withClaim("role", user.ROLE)
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRES_MINS * 60 * 1000))
                .sign(algorithm);
    }

    public static User extractUserFromToken(String token) throws InvalidTokenException {
        try {
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT jwt = verifier.verify(token);
            int id = jwt.getClaim("id").asInt();
            String name = jwt.getClaim("name").asString();
            String role = jwt.getClaim("role").asString();
            return new User(id, name, null, role);
        } catch (JWTVerificationException e) {
            throw new InvalidTokenException(e);
        }
    }

    public static String extendTokenExpiresDate(String token) {
        return generateTokenForUser(extractUserFromToken(token)); //wow it's amazing
    }

}
