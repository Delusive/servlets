package com.epam.servlets.exception;

public class InvalidTokenException extends RuntimeException {
    public InvalidTokenException(Throwable cause) {
        super(cause);
    }
}
