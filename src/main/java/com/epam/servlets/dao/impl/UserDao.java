package com.epam.servlets.dao.impl;

import com.epam.servlets.connection.IConnectionManager;
import com.epam.servlets.connection.impl.MySQLConnectionManager;
import com.epam.servlets.dao.IUserDao;
import com.epam.servlets.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;


public class UserDao implements IUserDao {
    private static final UserDao instance = new UserDao();
    private final Logger logger = LogManager.getLogger();
    private final IConnectionManager connectionManager = MySQLConnectionManager.getInstance();

    private UserDao() {
    }

    @Override
    public User getUserByNameAndPassword(String username, String password) {
        try (Connection connection = connectionManager.getConnection()) {
            String query =
                    "SELECT `users`.`id`, `users`.`name`, `users`.`password_hash`, `roles`.`name` AS 'role_name'" +
                            "FROM `users` " +
                            "JOIN `roles` ON `role_id` = `roles`.`id`" +
                            "WHERE `users`.`name` = ? AND `password_hash` = MD5(?);";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, username);
                preparedStatement.setString(2, password);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        return extractUserFromResultSet(resultSet);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return null;
    }

    @Override
    public User getUserByToken(UUID token) {
        try (Connection connection = connectionManager.getConnection()) {
            String query =
                    "SELECT `users`.`id`, `users`.`name`, `users`.`password_hash`, `roles`.`name` AS 'role_name'" +
                            "FROM `users` " +
                            "JOIN `roles` ON `role_id` = `roles`.`id`" +
                            "JOIN `tokens` ON `user_id` = `users`.`id`" +
                            "WHERE `token` = ?;";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, token.toString());
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        return extractUserFromResultSet(resultSet);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return null;
    }

    @Override
    public boolean registerUser(String name, String password, String role) {
        try (Connection connection = connectionManager.getConnection()) {
            String query = "INSERT INTO `users` (`name`, `password_hash`, `role_id`) " +
                    "VALUES(?, MD5(?), (" +
                    "SELECT `roles`.`id` FROM `roles` WHERE `roles`.`name` = ?" +
                    "));";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, password);
                preparedStatement.setString(3, role);
                preparedStatement.executeUpdate();
                return true;
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return false;
    }

    @Override
    public Boolean isUserExist(String username) {
        try (Connection connection = connectionManager.getConnection()) {
            String query = "SELECT `name` FROM `users` WHERE `name` = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, username);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    return resultSet.next();
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return null;
    }

    public static IUserDao getInstance() {
        return instance;
    }

    private User extractUserFromResultSet(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String username = resultSet.getString("name");
        String role = resultSet.getString("role_name");
        String passwordHash = resultSet.getString("password_hash");
        return new User(id, username, passwordHash, role);
    }
}
