package com.epam.servlets.dao;

import com.epam.servlets.entity.User;

import java.util.UUID;

public interface IUserDao {
    User getUserByNameAndPassword(String username, String password);
    User getUserByToken(UUID token);
    boolean registerUser(String name, String password, String role);
    Boolean isUserExist(String username);
}
