package com.epam.servlets.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger();
    private final String HOME_DIR = ServletsStatics.settings.getString("server.homedir");

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            Cookie cookie = new Cookie("jwt-token", "");
            cookie.setMaxAge(0);
            response.addCookie(cookie);
            logger.debug("Cookie cleared.");
            response.sendRedirect(HOME_DIR);
        } catch (IOException e) {
            logger.error(e);
        }
    }
}
