package com.epam.servlets.servlet.filter;

import com.epam.servlets.PropertiesManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

@WebFilter("/*")
public class VariableInjectingFilter implements Filter {
    private final String RUS_LOCALE_BAR = "<b>Русский</b> | <a href=\"#\" onclick=\"changeLocationTo('eng');\">English</a> | <a href=\"#\" onclick=\"changeLocationTo('ukr');\">Український</a>";
    private final String ENG_LOCALE_BAR = "<a href=\"#\" onclick=\"changeLocationTo('rus');\">Русский</a> | <b>English</b> | <a href=\"#\" onclick=\"changeLocationTo('ukr');\">Український</a>";
    private final String UKR_LOCALE_BAR = "<a href=\"#\" onclick=\"changeLocationTo('rus');\">Русский</a> | <a href=\"#\" onclick=\"changeLocationTo('eng');\">English</a> | <b>Український</b>";
    private final PropertiesManager enMessages = new PropertiesManager("/en_messages.properties");
    private final PropertiesManager ruMessages = new PropertiesManager("/ru_messages.properties");
    private final PropertiesManager ukrMessages = new PropertiesManager("/ukr_messages.properties");
    private final PropertiesManager settings = new PropertiesManager("/servlets.properties");

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //Локализация. Реализация оставляет желать лучшего:
        Optional<String> userLocale = getUserLocalization(servletRequest);
        String localizationChoiceBar = ENG_LOCALE_BAR;
        PropertiesManager messageSet = enMessages;
        if (userLocale.isPresent()) {
            switch (userLocale.get().toLowerCase()) {
                case "ru":
                    localizationChoiceBar = RUS_LOCALE_BAR;
                    messageSet = ruMessages;
                    break;
                default:
                case "en":
                    localizationChoiceBar = ENG_LOCALE_BAR;
                    messageSet = enMessages;
                    break;
                case "ukr":
                    localizationChoiceBar = UKR_LOCALE_BAR;
                    messageSet = ukrMessages;
                    break;
            }
        }
        servletRequest.setAttribute("localizationChoiceBar", localizationChoiceBar);
        servletRequest.setAttribute("messageSet", messageSet);
        //Этот кошмар закончился, все окей
        //System.out.println(ruMessages.getString("error.directaccess.error"));

        servletRequest.setAttribute("rootDir", settings.getString("server.homedir"));
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }

    private Optional<String> getUserLocalization(ServletRequest servletRequest) {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        for (Cookie cookie : httpRequest.getCookies()) {
            if (cookie.getName().equalsIgnoreCase("locale")) return Optional.of(cookie.getValue());
        }
        return Optional.empty();
    }
}
