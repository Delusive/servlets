package com.epam.servlets.servlet;

import com.epam.servlets.PropertiesManager;
import com.epam.servlets.dao.IUserDao;
import com.epam.servlets.dao.impl.UserDao;

public class ServletsStatics {
    static IUserDao userDao = UserDao.getInstance();
    //static PropertiesManager messages = new PropertiesManager("/ru_messages.properties");
    static PropertiesManager settings = new PropertiesManager("/servlets.properties");
}
