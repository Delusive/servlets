package com.epam.servlets.servlet;

import com.epam.servlets.JwtManager;
import com.epam.servlets.PropertiesManager;
import com.epam.servlets.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.servlets.servlet.ServletsStatics.userDao;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
    private final String HOME_DIR = ServletsStatics.settings.getString("server.homedir");

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username").trim();
        String password = request.getParameter("password");
        String repeatedPassword = request.getParameter("repeated-password");
        PropertiesManager messages = (PropertiesManager) request.getAttribute("messageSet");
        RequestDispatcher errorReqDispatcher = getServletContext().getRequestDispatcher("/error.jsp");
        request.setAttribute("returnLink", "/registration.jsp");
        if (!password.equals(repeatedPassword)) {
            request.setAttribute("errorMessage", messages.getString("registration.error.passwordsarenotidentical"));
            errorReqDispatcher.forward(request, response);
            return;
        }
        if ("".equals(username) || "".equals(password)) {
            request.setAttribute("errorMessage", messages.getString("registration.error.emptynameorpass"));
            errorReqDispatcher.forward(request, response);
            return;
        }
        if (userDao.isUserExist(username)) {
            request.setAttribute("errorMessage", messages.getString("registration.error.useralreadyregistered"));
            errorReqDispatcher.forward(request, response);
            return;
        }
        boolean isRegistered = userDao.registerUser(username, password, "BASE");
        if (isRegistered) {
            User user = userDao.getUserByNameAndPassword(username, password);
            String token = JwtManager.generateTokenForUser(user);
            response.addCookie(new Cookie("jwt-token", token));
            response.sendRedirect(HOME_DIR);
        } else {
            request.setAttribute("errorMessage", messages.getString("registration.error.internalerror"));
            errorReqDispatcher.forward(request, response);
        }
    }


}
