package com.epam.servlets.servlet;

import com.epam.servlets.JwtManager;
import com.epam.servlets.PropertiesManager;
import com.epam.servlets.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static com.epam.servlets.servlet.ServletsStatics.userDao;

@WebServlet("/auth")
public class AuthorizationServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger();
    private final String HOME_DIR = ServletsStatics.settings.getString("server.homedir");

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        PropertiesManager messages = (PropertiesManager) request.getAttribute("messageSet");
        try {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            Optional<User> user = Optional.ofNullable(userDao.getUserByNameAndPassword(username, password));
            if (user.isPresent()) {
                Cookie tokenCookie = new Cookie("jwt-token", JwtManager.generateTokenForUser(user.get()));
                response.addCookie(tokenCookie);
                response.sendRedirect(HOME_DIR);
            } else {
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/error.jsp");
                request.setAttribute("errorMessage", messages.getString("authorization.error.invalidnamepass"));
                request.setAttribute("returnLink", "/");
                rd.forward(request, response);
            }
        } catch (ServletException | IOException e) {
            logger.error(e);
        }

    }
}
