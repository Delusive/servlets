package com.epam.servlets.servlet;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.epam.servlets.JwtManager;
import com.epam.servlets.entity.User;
import com.epam.servlets.exception.InvalidTokenException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("") //root
public class HomePageServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            Optional<Cookie> cookie = getCookieByName(request.getCookies(), "jwt-token");
            String token = "";
            User user = null;
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/authorization.jsp");
            try {
                if (cookie.isPresent()) {
                    token = cookie.get().getValue();
                    user = JwtManager.extractUserFromToken(token);
                }
            } catch (TokenExpiredException e) {
                logger.debug("Token expired!");
                rd.forward(request, response);
                return;
            } catch (InvalidTokenException e) {
                logger.debug("Someone tried to auth using invalid token, aborted.");
                rd.forward(request, response);
                return;
            }
            if (user == null) {
                logger.debug("User not authorized");
                //rd = getServletContext().getRequestDispatcher("/authorization.jsp");
            } else {
                logger.debug("User authorized");
                setToken(response, JwtManager.extendTokenExpiresDate(token));
                rd = getServletContext().getRequestDispatcher("/userPage.jsp");
            }
            request.setAttribute("user", user);
            rd.forward(request, response);
        } catch (ServletException | IOException e) {
            logger.error(e);
        }
    }

    private Optional<Cookie> getCookieByName(Cookie[] cookies, String name) {
        if (cookies == null) return Optional.empty();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(name)) return Optional.of(cookie);
        }
        return Optional.empty();
    }

    private void setToken(HttpServletResponse response, String token) {
        response.addCookie(new Cookie("jwt-token", token));
    }
}
