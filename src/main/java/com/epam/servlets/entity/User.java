package com.epam.servlets.entity;

public class User extends Entity {
	public final int ID;
    public final String NAME;
    public final String PASSWORD_HASH;
    public final String ROLE;

    public User(int ID, String NAME, String PASSWORD_HASH, String ROLE) {
        this.ID = ID;
        this.NAME = NAME;
        this.PASSWORD_HASH = PASSWORD_HASH;
        this.ROLE = ROLE;
    }
}
