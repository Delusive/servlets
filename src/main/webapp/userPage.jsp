<%@ page import="com.epam.servlets.entity.User, com.epam.servlets.PropertiesManager" %>
<html>
<head>
    <title>Hello from localhost</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/script.js"></script>
</head>
<body>
<%
    User user = (User) request.getAttribute("user");
    PropertiesManager messages = (PropertiesManager)request.getAttribute("messageSet");
%>
<div class="container" id="user-page">
    <%if (user != null) {%>
    <div class="alert alert-primary" style="padding:20px"><%=messages.getString("userpage.greeting.beforename")%> <b><%=user.NAME%></b><%=messages.getString("userpage.greeting.aftername")%> <b><%=user.ROLE%></b><%=messages.getString("userpage.greeting.afterrole")%></div>
    <a href="<%=request.getAttribute("rootDir")%>/logout" class="btn btn-primary btn-danger btn-block" role="button"><%=messages.getString("userpage.logoutbtn.text")%></a>
    <%} else {%>
    <div class="alert alert-warning" style="padding:20px"><%=messages.getString("userpage.error.authorizationrequired")%></div>
    <a href="<%=request.getAttribute("rootDir")%>" class="btn btn-primary btn-block" role="button"><%=messages.getString("userpage.authorizebtn.text")%></a>
    <%}%>
</div>
</body>

<div class="footer">
    <div class="localization-bar"><%=request.getAttribute("localizationChoiceBar")%></div>
</div>

</html>