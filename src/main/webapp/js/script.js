//str
let changeLocationTo = (str) => {
    changeLocaleTo(str);
}

let changeLocaleTo = (str) => {
    switch (str.toLowerCase()) {
        case 'rus':
            document.cookie = "locale=ru";
            break;
        case 'eng':
            document.cookie = "locale=en";
            break;
        case 'ukr':
            document.cookie = "locale=ukr";
            break;
    }
    location.reload();
}