<%@ page import="com.epam.servlets.PropertiesManager" %>
<%@ page contentType="text/html;charset=utf-8" language="java" %>
<html>
<head>
    <title>Error!</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/script.js"></script>
</head>
<body>
<%
    PropertiesManager messages = (PropertiesManager)request.getAttribute("messageSet");
%>
<div id="error">
    <%if (request.getAttribute("errorMessage") == null) {%>

    <div class="alert alert-warning" role="alert"><%=messages.getString("errorpage.directaccess.error")%></div>
    <a href="<%=request.getAttribute("rootDir")%>" class="btn btn-primary btn-block"
       role="button"><%=messages.getString("errorpage.directaccess.error.button")%></a>

    <%} else {%>

    <div class="alert alert-danger" role="alert"><%=request.getAttribute("errorMessage")%>
    </div>
    <a href="<%=request.getAttribute("rootDir")%><%=request.getAttribute("returnLink")%>" class="btn btn-primary btn-block"
       role="button"><%=messages.getString("errorpage.errormessage.button")%></a>

    <%}%>
</div>

<div class="footer">
    <div class="localization-bar"><%=request.getAttribute("localizationChoiceBar")%></div>
</div>

</body>
</html>