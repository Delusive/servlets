<%@ page import="com.epam.servlets.PropertiesManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/script.js"></script>
</head>
<body>
<%
    PropertiesManager messages = (PropertiesManager)request.getAttribute("messageSet");
%>
<div id="register-form" class="container">
    <form method="post" action="<%=request.getAttribute("rootDir")%>/register">
        <div class="form-group">
            <label for="username"><%=messages.getString("registration.form.username")%></label>
            <input type="text" class="form-control" id="username" placeholder="<%=messages.getString("registration.form.username.placeholder")%>" name="username">
        </div>
        <div class="form-group">
            <label for="password"><%=messages.getString("registration.form.password")%></label>
            <input type="password" class="form-control" id="password" placeholder="<%=messages.getString("registration.form.password.placeholder")%>" name="password">
        </div>
        <div class="form-group">
            <label for="repeated-password"><%=messages.getString("registration.form.repeatpassword")%></label>
            <input type="password" class="form-control" id="repeated-password" placeholder="<%=messages.getString("registration.form.repeatpassword.placeholder")%>"
                   name="repeated-password">
        </div>
        <button type="submit" class="btn btn-primary btn-block"><%=messages.getString("registration.form.submit")%></button>
    </form>
</div>

<div class="footer">
    <div class="localization-bar"><%=request.getAttribute("localizationChoiceBar")%></div>
</div>

</body>
</html>
