<%@ page import="com.epam.servlets.PropertiesManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>jsp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/script.js"></script>
</head>
<body>
<%
    PropertiesManager messages = (PropertiesManager) request.getAttribute("messageSet");
%>
<div id="auth" class="container">
    <form method="post" action="<%=request.getAttribute("rootDir")%>/auth">
        <div class="form-group">
            <label for="username"><%=messages.getString("authorization.form.username")%></label>
            <input type="text" class="form-control" id="username" placeholder="<%=messages.getString("authorization.form.username.placeholder")%>" name="username">
        </div>
        <div class="form-group">
            <label for="password"><%=messages.getString("authorization.form.password")%></label>
            <input type="password" class="form-control" id="password" placeholder="<%=messages.getString("authorization.form.password.placeholder")%>" name="password">
        </div>
        <button type="submit" class="btn btn-primary btn-block"><%=messages.getString("authorization.form.submit.login")%></button>
        <a href="<%=request.getAttribute("rootDir")%>/registration.jsp" class="btn btn-secondary btn-block"
           role="button" style="margin-top: 5px;"><%=messages.getString("authorization.form.link.register")%></a>
    </form>

</div>

<div class="footer">
    <div class="localization-bar"><%=request.getAttribute("localizationChoiceBar")%></div>
</div>

</body>
</html>